import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 全局引入axios 并配置
import axios from 'axios'
// 引入公共js
import commonJS from './plugins/common.js'
import vant from 'vant'
import { Dialog } from 'vant'
import { Toast } from 'vant'
import 'vant/lib/index.css';
import './assets/style/common.less';

Vue.use(vant)

Vue.prototype.$axios = axios
Vue.prototype.$dialog = Dialog
Vue.prototype.$toast = Toast
Vue.prototype.$common = commonJS

Vue.config.productionTip = false

// 全局引入qs
import qs from 'qs'
Vue.prototype.$qs = qs;

// 引入vconsole
// import VConsole from 'vconsole';
// let vConsole = new VConsole();

// 引入 WebViewJavascriptBridge
import Bridge from './bridge.js'
Vue.prototype.$bridge = Bridge



var newVue = new Vue({
  router,
  store,
  render (createElement) {
    let props = {}
    this.$route.meta.showNav ? props.layout = 'nav-layout' : props.layout = 'blank-layout'
    this.$route.meta.nav2 ? props.layout = 'nav-layout2' : null
    this.$route.meta.navDocument ? props.layout = 'nav-document' : null
    this.$route.meta.navCar ? props.layout = 'nav-car' : null
    return createElement(App , {props})
  }
}).$mount('#app')
export default newVue

/** ****************axios拦截器****************/
// request 拦截 添加公用url头
Vue.prototype.$axios.interceptors.request.use(
  config => {
    // console.log(config)
    // let token = sessionStorage.getItem("token")
    // //存在token
    // if(token){
    //   //post方式处理
    //   if(config.method === "post"){
    //       //不存在参数
    //       if(!config.hasOwnProperty('data')){
    //         config['data'] = JSON.stringify({token: token})
    //       } else {
    //         let _data = config.data
    //         _data['token'] = token
    //         config.data = JSON.stringify(_data)
    //       }
    //   }
    //   //get方式处理
    //   else if(config.method === "get"){
    //      //不存在参数
    //     if(!config.hasOwnProperty('params')){
    //       config['params'] ={token: token}
    //     } else {
    //       //存在参数
    //       config.params['token'] = token
    //     }
    //   }
    // }

    // try {
    //   let array = Object.keys(config.data)
    //   array.forEach(item => {
    //     if(typeof(config.data[item]) == 'string') {
    //       //去掉左右空格
    //       config.data[item] = config.data[item].LTrim()
    //       config.data[item] = config.data[item].RTrim()
    //     }
    //   })
    // } catch(error) {
    //   console.log(error)
    // }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// response 拦截
Vue.prototype.$axios.interceptors.response.use(
  (response) => {
    // 请求成功的拦截
    if (response.data.code*1 == 200) {
      return response.data
    } else {
      switch (response.data.code*1) {
        case -312:
        case -351:
        case -352:
        case -400: // 用户登录信息异常, 在页面初始化时验证
        case -411: // 用户登录信息异常, 在页面初始化时验证
        case -412:
        case -413:
        case -419: 
        case -422: // 用户登录信息异常, 在页面初始化时验证
        case -454:
        case -456:
        case -1002:
        case -2003:
          return Promise.reject(response.data || '未成功, 接口请求返回异常');
        default:
          // 未注册的code码将响应到全局错误中去
          vueGlobal.$bridge.callHandler('requestError')
          return Promise.reject(response.data)// 数据中包含msg
      }
    }
    /* if (response.config.responseType != "blob") {  //非文件类型不进行处理
      if (response.data.meta.code == "401") {
        // 抛出未登录异常, 并定时跳转至登录页
        setTimeout(() => {
          window.location.href = window.location.origin + "/web/login"
        }, 1500)
        return Promise.reject("未登陆或登陆信息失效!")
      } else if (response.data.meta.code != 0) {
        // 抛出请求失的异常
        return Promise.reject(response.data.meta.message)
      } else {
        // 无异常直接返回
        return response
      }
    } else { //文件类型直接返回
      return response
    } */
    // 当meta信息为空时，表示后台回传的文件流
    // if(!response.data.meta) return response;

    // if (response.data.meta.code != 0) {

    //   if(response.data.meta.code == 401 || response.data.meta.code == 405) {
    //     setTimeout(() => {
    //       window.location.href = window.location.origin + "/web/login"
    //     }, 1500)
    //     return Promise.reject("登录验证失效, 请重新登录");
    //   } else if (response.data.meta.code == 403) {
    //     setTimeout(() => {
    //       window.location.href = window.location.origin + "/web/login"
    //     }, 2000)
    //     return Promise.reject("您的权限已被修改, 请重新登录");
    //   } else {
    //     return Promise.reject(response.data.meta.message)
    //   }
    // } else {
    //   return response
    // }
  },
  error => {
    if (error.response && error.response.data && error.response.data.msg) {
      vueGlobal.$bridge.callHandler('requestError')
      return Promise.reject(error.data.msg)
    } else { // 请求没有返回体, 请求失败
      vueGlobal.$bridge.callHandler('requestError')
      return Promise.reject({ msg: '网络请求错误, 请联系管理员' })
    }
  }
)


