import Vue from 'vue'
import VueRouter from 'vue-router'
import { Dialog } from 'vant'
import Bridge from '../bridge.js'

const test = () => import('../views/Test.vue')
const test2 = () => import('../views/Test2.vue')
// 消息
const meetingIndex = () => import('../views/notification/MeetingIndex.vue')       //
const meetingDetail = () => import('../views/notification/MeetingDetail.vue')     //会议详情
const noticeDetail = () => import('../views/notification/NoticeDetail.vue')       //通知详情

// 会议室管理
const meetingManage = () => import('../views/meeting/MeetingManage.vue')          //会议室管理首页
const meetDetail = () => import('../views/meeting/MeetDetail.vue')                //会议室预约详情
const allPeople = () => import('../views/meeting/AllPeople.vue')                  //参会人员
const detailInfo = () => import('../views/meeting/DetailInfo.vue')                //我的会议详情
const approvalMeeting = () => import('../views/meeting/ApprovalMeeting.vue')      //会议审批页
const approvalDetail = () => import('../views/meeting/ApprovalDetail.vue')        //会议审批详情
const myMeeting = () => import('../views/meeting/MyMeeting.vue')                  //我的会议页

// 我的
const myInformation = () => import('../views/mine/MyInformation.vue')             //个人信息
const resetPwd = () => import('../views/mine/ResetPwd.vue')                       //重置密码页
const knowledge = () => import('../views/mine/Knowledge.vue')                     //知识管理页
const knowledgeDetail = () => import('../views/mine/KnowledgeDetail.vue')         //知识管理详情
const library = () => import('../views/mine/Library.vue')                         //图书馆
const libraryDetail = () => import('../views/mine/LibraryDetail.vue')             //图书馆详情
const journalDetail = () => import('../views/mine/JournalDetail.vue')             //期刊详情
const rules = () => import('../views/mine/Rules.vue')                             //规章制度

// 开发页
const blank = () => import('../views/Blank.vue')

//日程
const addSchedule = () => import('../views/schedule/AddSchedule.vue')             //日程添加页
// 工作日志
const workJournal = () => import('../views/schedule/WorkJournal.vue')             //工作日志首页
const addJournal = () => import('../views/schedule/AddJournal.vue')               //添加工作日志
const logDetail = () => import('../views/schedule/LogDetail.vue')                 //日志详情

// 工作审批
const myApproval = () => import('../views/approval/MyApproval.vue')               //审批工作流首页
const myStartApproval = () => import('../views/approval/MyStartApproval.vue')     //我的审批
const initiateApproval = () => import('../views/approval/InitiateApproval.vue')   //新建审批页
const approvalForm = () => import('../views/approval/ApprovalForm.vue')           //审批表单
const approvalInfo = () => import('../views/approval/ApprovalInfo.vue')           //审批详情

// 公文
const documentsHandle = () => import('../views/documents/DocumentsHandle.vue')    //公文首页
const documentsWrite  = () => import('../views/documents/DocumentsWrite.vue')     //新建公文
const documentsForm  = () => import('../views/documents/DocumentsForm.vue')       //公文表单
const handleList  = () => import('../views/documents/HandleList.vue')             //公文办理页
const handleDetail  = () => import('../views/documents/HandleDetail.vue')         //公文办理详情
const readyList  = () => import('../views/documents/ReadyList.vue')               //公文阅读页
const readyDetail  = () => import('../views/documents/ReadyDetail.vue')           //公文阅读详情
const myDocument  = () => import('../views/documents/MyDocument.vue')             //我的公文
const myDocDetail  = () => import('../views/documents/MyDocDetail.vue')           //我的公文详情
const monitor  = () => import('../views/documents/Monitor.vue')                   //公文监控
const monitorDetail  = () => import('../views/documents/MonitorDetail.vue')       //公文监控详情

// 历史流程(
const historyFlowList  = () => import('../views/history/HistoryFlowList.vue')     //历史流程
const historyFlowDetail  = () => import('../views/history/HistoryFlowDetail.vue') //历史流程详情

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    redirect: '/meetingManage'
  },
  {
    path: '/test',
    name: 'test',
    meta: {
      showNav: false,
    },
    component: test
  },
  {
    path: '/test2',
    name: 'test2',
    meta: {
      showNav: true,
    },
    component: test2
  },

  // 消息
  {
    path: '/meetingIndex',
    name: 'meetingIndex',
    component: meetingIndex
  },
  {
    path: '/meetingDetail',
    routerName:'邀请会议详情',
    name: 'meetingDetail',
    component: meetingDetail
  },
  {
    path: '/noticeDetail',
    routerName:'通知、公告详情',
    name: 'noticeDetail',
    component: noticeDetail
  },

  // 会议室
  {
    path: '/meetingManage',
    routerName:'会议室管理',
    name: 'meetingManage',
    meta: {
      showNav: true
    },
    component: meetingManage
  },
  {
    path: '/meetDetail',
    routerName:'会议室详情',
    name: 'meetDetail',
    component: meetDetail
  },
  {
    path: '/allPeople',
    routerName:'参会人员',
    name: 'allPeople',
    component: allPeople
  },
  {
    path: '/detailInfo',
    routerName:'查看会议室详情',
    name: 'detailInfo',
    component: detailInfo
  },
  {
    path: '/myMeeting',
    routerName:'我的会议',
    name: 'myMeeting',
    component: myMeeting,
    meta: {
      showNav: true
    },
  },
  {
    path: '/approvalMeeting',
    routerName:'会议室审批',
    name: 'approvalMeeting',
    component: approvalMeeting,
    meta: {
      showNav: true
    },
  },
  {
    path: '/approvalDetail',
    routerName:'会议管理详情',
    name: 'approvalDetail',
    component: approvalDetail
  },

  // 我的
  {
    path: '/myInformation',
    routerName:'我的资料',
    name: 'myInformation',
    component: myInformation
  },
  {
    path: '/resetPwd',
    routerName:'我的资料',
    name: 'resetPwd',
    component: resetPwd
  },
  {
    path: '/knowledge',
    routerName:'知识管理',
    name: 'knowledge',
    component: knowledge
  },
  {
    path: '/knowledgeDetail',
    routerName:'知识详情',
    name: 'knowledgeDetail',
    component: knowledgeDetail
  },
  {
    path: '/library',
    routerName:'图书管理',
    name: 'library',
    component: library
  },
  {
    path: '/libraryDetail',
    routerName:'图书详情',
    name: 'libraryDetail',
    component: libraryDetail
  },
  {
    path: '/journalDetail',
    routerName:'期刊详情',
    name: 'journalDetail',
    component: journalDetail
  },
  {
    path: '/rules',
    routerName:'规章制度',
    name: 'rules',
    component: rules
  },
  // 开发页
  {
    path: '/blank',
    routerName:'开发页',
    name: 'blank',
    component: blank
  },
  // 日程
  {
    path: '/addSchedule',
    routerName:'日程',
    name: 'addSchedule',
    component: addSchedule
  },
  // 工作日志
  {
    path: '/workJournal',
    routerName:'工作日志',
    name: 'workJournal',
    component: workJournal
  },
  {
    path: '/addJournal',
    routerName:'写工作日志',
    name: 'addJournal',
    component: addJournal
  },
  {
    path: '/logDetail',
    routerName:'日志详情',
    name: 'logDetail',
    component: logDetail
  },
  // 工作审批
  {
    path: '/myApproval',
    routerName:'我审批的',
    name: 'myApproval',
    component: myApproval,
    meta: {
      showNav: true,
      nav2: true
    }
  },
  {
    path: '/myStartApproval',
    routerName:'我发起的',
    name: 'myStartApproval',
    component: myStartApproval,
    meta: {
      showNav: true,
      nav2: true
    }
  },
  {
    path: '/initiateApproval',
    routerName:'发起审批',
    name: 'initiateApproval',
    component: initiateApproval,
    meta: {
      showNav: true,
      nav2: true
    }
  },
  {
    path: '/approvalForm',
    routerName:'发起审批',
    name: 'approvalForm',
    component: approvalForm,
  },
  {
    path: '/approvalInfo',
    routerName:'流程审批',
    name: 'approvalInfo',
    component: approvalInfo,
  },
  {
    path: '/documents/handle',
    routerName: '办理公文',
    name: 'documentsHandle',
    component: documentsHandle,
    meta: {
      navDocument: true,
    }
  },
  {
    path: '/documents/write',
    routerName: '公文拟稿',
    name: 'documentsWrite',
    component: documentsWrite,
    meta: {
      navDocument: true,
    }
  },
  {
    path: '/documentsForm',
    routerName:'公文拟稿表单',
    name: 'documentsForm',
    component: documentsForm,
  },
  {
    path: '/handleList',
    routerName:'公文办理',
    name: 'handleList',
    component: handleList,
  },
  {
    path: '/handleDetail',
    routerName:'公文办理详情',
    name: 'handleDetail',
    component: handleDetail,
  },
  {
    path: '/readyList',
    routerName:'公文阅读',
    name: 'readyList',
    component: readyList,
  },
  {
    path: '/readyDetail',
    routerName:'公文阅读详情',
    name: 'readyDetail',
    component: readyDetail,
  },
  {
    path: '/myDocument',
    routerName:'我的公文',
    name: 'myDocument',
    component: myDocument,
  },
  {
    path: '/myDocDetail',
    routerName:'我的公文详情',
    name: 'myDocDetail',
    component: myDocDetail,
  },
  {
    path: '/monitor',
    routerName:'我的监控',
    name: 'monitor',
    component: monitor,
  },
  {
    path: '/monitorDetail',
    routerName:'我的监控详情',
    name: 'monitorDetail',
    component: monitorDetail,
  },
  {
    path: '/vote',
    routerName:'投票',
    name: 'vote',
    component: () => import('../views/vote/List.vue'),
  },
  {
    path: '/voteDetail',
    routerName:'投票详情',
    name: 'voteDetail',
    component: () => import('../views/vote/Detail.vue'),
  },
  {
    path: '/news',
    routerName:'新闻',
    name: 'news',
    component: () => import('../views/news/List.vue'),
  },
  {
    path: '/newsDetail',
    routerName:'新闻详情',
    name: 'newsDetail',
    component: () => import('../views/news/Detail.vue'),
  },
  {
    path: '/about',
    routerName:'关于文档详情',
    name: 'about',
    component: () => import('../views/about/Document.vue'),
  },
  {
    path: '/historyFlowList',
    routerName:'历史流程',
    name: 'historyFlowList',
    component: historyFlowList,
  },
  {
    path: '/historyFlowDetail',
    routerName:'历史流程详情',
    name: 'historyFlowDetail',
    component: historyFlowDetail,
  },
  {
    path: '/salary',
    routerName:'工资条',
    name: 'salary',
    component: () => import('../views/Salary.vue'),
  },
  {
    path: '/richtext',
    routerName:'富文本',
    name: 'richText',
    component: () => import('../views/ViewRichText.vue'),
  },
  {
    path: '/gojsChart',
    routerName: '图表',
    name: 'gojsChart',
    component: () => import('../views/GojsChart.vue'),
  },
  {
    path: '/hasRead',
    routerName:'已阅人员',
    name: 'hasRead',
    component: () => import('../views/news/HasRead.vue'),
  },
  {
    path: '/carLog',
    routerName:'派车消息',
    name: 'carLog',
    meta: {
      navCar: true
    },
    component: () => import('../views/car/Log.vue'),
  },
  {
    path: '/carManage',
    routerName:'派车管理',
    name: 'carManage',
    meta: {
      navCar: true
    },
    component: () => import('../views/car/Manage.vue'),
  },
  {
    path: '/myCar',
    routerName:'我的用车',
    name: 'myCar',
    meta: {
      navCar: true
    },
    component: () => import('../views/car/Mine.vue'),
  },
  {
    path: '/carDetail',
    routerName:'用车详情',
    name: 'carDetail',
    component: () => import('../views/car/Detail.vue'),
  },
  {
    path: '/carForm',
    routerName:'派车表单',
    name: 'carForm',
    component: () => import('../views/car/Form.vue'),
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

router.onError(error => {
  Bridge.callHandler('navBack')
  Bridge.registerHandler('navBackClicked', (data, callBack) => {
    console.log('返回已被拦截, 无法再返回, 需要点击dialog的 确定 按钮')
  })
  Dialog({ message: '从服务器获取资源出错, 请重新进入本页面!' }).then(_ => {
    Bridge.callHandler('deleteWebCache')
  });
})
// router.beforeEach((to, from, next) => {
//   // ...
//   next(() => {
//     console.log(121)
//   })
// })

// router.onReady(error => {
//   // ...
//   console.log(error)
// })

export default router
