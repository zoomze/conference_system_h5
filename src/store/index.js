import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    from: '', // 仅在初次created被修改 , 后续不做修改
    userId: '',
    token: '',
    activeNav: 0, //layout在切换会导致注册在navlayout中的监听不生效, 故移至全局
    myMeeting: 0,
    noStart:   0,
    isStart:   0,
    voted:     true, // 是否进行过投票, 初始为true 用于初始刷新
    approvalBack: '', //审批详情下一步处理自动返回到列表
    documentBack: '', //公文详情下一步处理自动返回到列表,
    hasRead: -1, // 已经阅读的项目Index
    newsScroll: 0 //新闻卷去的高度
  },
  mutations: {
    /**
     * 根据key值修改数据
     * @param  {[type]} state [description]
     * @param  {[type]} data  [description]
     * @return {[type]}       [description]
     */
    changeStatus(state, data) {
      console.log(data.value)
      state[data.key] = data.value
    },
  },
  actions: {
  },
  modules: {
  }
})
