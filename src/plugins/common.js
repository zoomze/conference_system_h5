import vue from 'vue'
import router from '.././router'
import store from '.././store'
import qs from 'qs'
import thisVue from '../main.js'

let common = {
  // basePath: `${window.location.origin}/apis/`, // 本地开发测试url
  
  // basePath:   `http://oagaosun.yuyankeji.cn/app/`, //高驰oa 测试服务器
  // filePath:   `https://yuyanoa.yuyankeji.cn/`, // 测试 文件服务器地址

  uploadPath: `${window.location.origin}/apis/admin/activity/wangEditorImg`, // 不用修改
  
  // basePath:   `http://218.206.180.131:2180/app/`, //高驰oa 正式服务器
  // filePath:   `http://218.206.180.131:2180/`, // 正式 文件服务器地址

  // basePath:   `http://nimaoa.nbtool.top/app/`, // 尼玛oa 测试接口地址
  // filePath:   `http://nimaoa.nbtool.top/`, // 尼玛oa 测试文件服务器地址

  // basePath:   `http://59.217.45.12:5080/app/`, // 尼玛oa正式接口地址
  // filePath:   `http://59.217.45.12:5080/`, // 尼玛oa正式文件服务器地址
  
  // basePath: `http://47.105.40.119:8080/app/`, // 新47
  // filePath: `http://47.105.40.119:8080/`,

  // basePath: `http://113.62.180.44:2180/app/`, //高驰迁移后的服务器地址（高驰现在用的地址）
  // filePath: `http://113.62.180.44:2180/`,

  basePath: `http://221.13.83.4:2180/app/`, //高驰迁移后的服务器地址（高驰新地址）
  filePath: `http://221.13.83.4:2180/`,
  
  // basePath: `http://appstore.yuyankeji.cn/app/`, //appstore审核
  // filePath: `http://appstore.yuyankeji.cn/`,

  emailReg:   /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/,
  mobileReg:  /^1[34578]\d{9}$/,
  // 6到12位数字或字母
  normalPassword: /^[0-9A-Za-z]{6,12}$/,
  // 纯数字
  number: /^[0-9]+.?[0-9]*$/,
  /**
   * 在会议下 点击了 返回 按钮
   * @param  {String} param 需要返回的页面名称 不传参数则认定为调用vuex中的from 缺省为历史回滚
   * @return {[type]}       [description]
   */
  backButtonClicked(param) {
    switch(param ? param : thisVue.$store.state.from) {
      case 'schedule':
        thisVue.$bridge.callHandler('backToSchedule', {}, (data) => {
          console.log("已调用 backToSchedule 方法")
        })
        break;
      case 'message':
        thisVue.$bridge.callHandler('backToMessage', {}, (data) => {
          console.log("已调用 backToMessage 方法")
        })
        break;
      case 'dashboard':
        thisVue.$bridge.callHandler('backToDashboard', {}, (data) => {
          console.log("已调用 backToDashboard 方法")
        })
        break;
      case 'mine':
        thisVue.$bridge.callHandler('backToMine', {}, (data) => {
          console.log("已调用 backToMine 方法")
        })
        break;
      case 'back': //强行控制历史回滚
        thisVue.$router.go(-1)
        break;
      default:
        thisVue.$router.go(-1)
        break
    }
  },
  /**
   * 遍历树(递归遍历), 删除空白的childrens
   * @param  {object}   node 根节点(单个对象, 多个树需要重复调用)
   * @return none
   */
  traverseTree (node) {
    if (!node) {
      return
    }
    if (node.childrens && node.childrens.length > 0) {
      var i = 0
      for (i = 0; i < node.childrens.length; i++) {
        this.traverseTree(node.childrens[i])
      }
    } else {
      delete node.childrens
    }
  },
  /**
   * 字符串通过','分割为数组
   * @param str String 字符串
   * @param  {object}   node 根节点(单个对象, 多个树需要重复调用)
   * @return {Array}
   */
  strSplitArrayBydou (str) {
    if (!str) {
      return []
    } else if (str.indexOf(',') !== -1) {
      return str.split(',')
    } else {
      return [str]
    }
  },
  /**
   * 时间戳转化为年 月 日 时 分 秒
   * number: 传入时间戳
   * format：返回格式，支持自定义，但参数必须与formateArr里保持一致 默认'Y-M-D h:m'
   */
  convertDate (number, format='Y-M-D h:m') {
    let  formateArr  = ['Y','M','D','h','m','s']
    let returnArr   = []
    if(number.toString().length == 10) {
      var date = new Date(number * 1000)
    } else if (number.toString().length == 13) {
      var date = new Date(number)
    } else {
      var date = new Date(number * 1000)
      // return '错误的时间戳精度'
    }
    
    returnArr.push(date.getFullYear())
    returnArr.push(this.formatNumber(date.getMonth() + 1))
    returnArr.push(this.formatNumber(date.getDate()))
    returnArr.push(this.formatNumber(date.getHours()))
    returnArr.push(this.formatNumber(date.getMinutes()))
    returnArr.push(this.formatNumber(date.getSeconds()))

    for (const i in returnArr)
    {
      format = format.replace(formateArr[i], returnArr[i])
    }
    return format
    // let time = timeStamp.toString()
    // let resultTime
    // time.length == 10 ? resultTime = parseInt(time) * 1000 : resultTime = parseInt(time)
    //
    // return timeStamp ? new Date(resultTime).Format(format || 'yyyy-MM-dd hh:mm') : ''
  },
  /**
   * 时间戳转化为年 月 日 时 分 秒
   * number: 传入时间戳
   * format：返回格式，支持自定义，但参数必须与formateArr里保持一致 默认'Y-M-D h:m'
   */
   formatTime(number, format='Y-M-D h:m') {
    let  formateArr  = ['Y','M','D','h','m','s']
    let returnArr   = []

    var date = new Date(number)
    returnArr.push(date.getFullYear())
    returnArr.push(this.formatNumber(date.getMonth() + 1))
    returnArr.push(this.formatNumber(date.getDate()))
    returnArr.push(this.formatNumber(date.getHours()))
    returnArr.push(this.formatNumber(date.getMinutes()))
    returnArr.push(this.formatNumber(date.getSeconds()))

    for (const i in returnArr)
    {
      format = format.replace(formateArr[i], returnArr[i])
    }
    return format
  },
  /**
   * 时间转为秒
   * @param time 时间(00:00)
   * @returns {string} 时间戳（单位：秒）
   */
  timeToSec(time) {
      var s = '';

      var hour = time.split(':')[0];
      var min = time.split(':')[1];

      s = Number(hour*3600) + Number(min*60);

      return s;
  },
   /**
     * 时间秒数格式化
     * @param s 时间戳（单位：秒）
     * @returns {*} 格式化后的时分秒
     */
    secToTime(s) {
        var t;
        if(s > -1){
            var hour = Math.floor(s/3600);
            var min = Math.floor(s/60) % 60;
            if(hour < 10) {
                t = '0'+ hour + ":";
            } else {
                t = hour + ":";
            }

            if(min < 10){t += "0";}
            t += min;
        }
        return t;
    },
  /**
   * 只取时分方法
   * @param  {[type]} str [Y-M-D h:m:s的字符串数据]
   * @return {[type]}     [返回 h:m]
   */
  dateToTime(str) {
    if (str) {
      let arr = str.split(" ")
      let times = arr[1].split(":")
      let format = times[0]+":"+times[1]
      return format
    }
  },
  /**
   * 根据type不同类型返回对应的图片
   * @return {[type]} [description]
   */
  fileIcon(type) {
    let iconImg = ""
    switch(type) {
      case 1:
      case 'png':
      case 'jpg':
      case 'jpeg':
      case 'gif':
        iconImg = require('../assets/mine/img.png')
        break;
      case 2:
      case 'doc':
      case 'docx':
        iconImg = require('../assets/mine/word.png')
        break;
      case 3:
      case 'xls':
      case 'xlsx':
        iconImg = require('../assets/mine/xsl.png')
        break;
      case 4:
      case 'ppt':
      case 'pptx':
        iconImg = require('../assets/mine/ppt.png')
        break;
      case 5:
      case 'pdf':
        iconImg = require('../assets/mine/pdf.png')
        break;
      case 7:
      case 'zip':
      case 'rar':
      case '7z':
        iconImg = require('../assets/mine/zip.png')
        break;
      case 8:
      case 'txt':
        iconImg = require('../assets/mine/txt.png')
        break;
      case 9:
      default:
        iconImg = require('../assets/mine/wenhao.png')
        break;
    }
    return iconImg
  },
  /**
   * 返回假期类型的名字
   * @return {[type]} [description]
   */
  returnVacationName(type) {
    let name = "";
    type = '' + type
    switch(type) {
      case '1':
        name = '非医疗病假'
        break;
      case '2':
        name = '事假'
        break;
      case '3':
        name = '婚假'
        break;
      case '4':
        name = '产假'
        break;
      case '5':
        name = '陪产假'
        break;
      case '6':
        name = '年假'
        break;
      case '7':
        name = '工伤假'
        break;
      case '8':
        name = '生育假'
        break;
      case '9':
        name = '丧假'
        break;
      case '10':
        name = '备用假'
        break;
      case '11':
        name = '医疗病假'
        break;
      default:
        name = ''
    }
    return name
  },
  /**
   * 历史流程
   * 根据type不同类型返回对应的图片
   * @return {[type]} [description]
   */
  historyFileIcon(type) {
    let iconImg = ""
    switch(type) {
      case '1':
        iconImg = require('../assets/mine/img.png')
        break;
      case '2':
      case '11':
        iconImg = require('../assets/mine/img.png')
        break;
      case '3':
      case '6':
      case '7':
      case '10':
        iconImg = require('../assets/mine/word.png')
        break;
      case '4':
      case '8':
        iconImg = require('../assets/mine/xsl.png')
        break;
      case '5':
      case '9':
        iconImg = require('../assets/mine/ppt.png')
        break;
      case '6':
        iconImg = require('../assets/mine/pdf.png')
        break;
      default:
        iconImg = require('../assets/mine/wenhao.png')
        break;
    }
    return iconImg
  },
  /*
  *数据转化
   */

  formatNumber(n) {
    n = n.toString()
    return n[1] ? n : '0' + n
  },
  /**
   * 数字转化为大写汉字
   * @param  {[type]} str [description]
   * @return {[type]}     [description]
   */
  numberChinese(str) {
    if(parseInt(str) > 9999999999999) {
      return '超出支持的最大金额'
    } else {
      var num = parseFloat(str);
      var strOutput = "",
          strUnit = '万仟佰拾亿仟佰拾万仟佰拾元角分';
      num += "00";
      var intPos = num.indexOf('.'); //小数点位置
      if (intPos >= 0){
        num = num.substring(0, intPos) + num.substr(intPos + 1, 2);
      }
      // 获取单位字符串 长度
      strUnit = strUnit.substr(strUnit.length - num.length);
      for (var i=0; i < num.length; i++){
        strOutput += '零壹贰叁肆伍陆柒捌玖'.substr(num.substr(i,1),1) + strUnit.substr(i,1);
      }
      return strOutput.replace(/零角零分$/, '整')
        .replace(/零[仟佰拾]/g, '零')
        .replace(/零{2,}/g, '零')
        .replace(/零([亿|万])/g, '$1')
        .replace(/零+元/, '元')
        .replace(/亿零{0,3}万/, '亿')
        .replace(/^元/, "零元")
    }
    
  },
  /*
  *判断操作系统
   */
  detectOS () {
    let operatingInfo = navigator.userAgent
    let isWin = (navigator.platform == "Win32") || (navigator.platform == "Windows")
    if(isWin)
    return "Windows"
    let isMac = (navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh") || (navigator.platform == "MacIntel");
    if (isMac) return "Mac"
    let isUnix = (navigator.platform == "X11") && !isWin && !isMac
    if (isUnix) return "Unix"
    let isLinux = (String(navigator.platform).indexOf("Linux") > -1)
    if (isLinux) return "Linux"
  },
  /*
   * 性别转文字
   */
  transferGender (sex) {
    return sex == 3 ? '保密' : sex == 1 ? '男' : '女'
  },
  lengthValidate () {

  },
  /**
   * 根据日期字符串获取星期几
   * @param dateString 日期字符串（如：2016-12-29），为空时为用户电脑当前日期
   * @returns {String}
   */
  getWeek (dateString) {
    var date
    date = new Date(dateString)
    return '星期' + '日一二三四五六'.charAt(date.getDay())
  },
  /*
   * 功能：计算当前时间（或指定时间），向前推算周数(weekcount)，得到结果周的第一天的时期值；
   * 参数：
   * mode -推算模式（'cn'表示国人习惯【周一至周日】；'en'表示国际习惯【周日至周一】）
   * weekcount -表示周数（0-表示本周， 1-前一周，2-前两周，以此推算）；
   * end -指定时间的字符串（未指定则取当前时间）；
   */
  nearlyWeeks (mode, weekcount, end) {
      if (mode == undefined) mode = "cn";
      if (weekcount == undefined) weekcount = 0;
      if (end != undefined)
          end = new Date(new Date(end).toDateString());
      else
          end = new Date(new Date().toDateString());

      var days = 0;
      if (mode == "cn")
          days = (end.getDay() == 0 ? 7 : end.getDay()) - 1;
      else
          days = end.getDay();

      return new Date(end.getTime() - (days + weekcount * 7) * 24 * 60 * 60 * 1000);
  },

  /*
   * 功能：计算一段时间内工作的天数。不包括周末和法定节假日，法定调休日为工作日，周末为周六、周日两天；
   * 参数：
   * mode -推算模式（'cn'表示国人习惯【周一至周日】；'en'表示国际习惯【周日至周一】）
   * beginDays -时间段开始日期(比如：2020-05-18)；
   * endDays -时间段结束日期(比如：2020-05-18)；
   * flag - 是否要删除节假日和周末
   * Holiday -节假日
   * WeekendsOff -调休日
   */
  getWorkDayCount (mode, beginDays, endDays, flag, Holiday, WeekendsOff) {
      let beginDay = new Date(beginDays)
      let endDay = new Date(endDays)
      let begin = new Date(beginDay.toDateString());
      let end = new Date(endDay.toDateString());

      //每天的毫秒总数，用于以下换算
      let daytime = 24 * 60 * 60 * 1000;
      //两个时间段相隔的总天数
      let days = (end - begin) / daytime + 1;

      let weekEndCount = 0; //周末天数默认0
      if (flag) {
          //时间段起始时间所在周的第一天
          let beginWeekFirstDay = this.nearlyWeeks(mode, 0, beginDay.getTime()).getTime();
          //时间段结束时间所在周的最后天
          let endWeekOverDay = this.nearlyWeeks(mode, 0, endDay.getTime()).getTime() + 6 * daytime;

          //由beginWeekFirstDay和endWeekOverDay换算出，周末的天数
          weekEndCount = ((endWeekOverDay - beginWeekFirstDay) / daytime + 1) / 7 * 2;
          //根据参数mode，调整周末天数的值
          if (mode == "cn") {
              if (endDay.getDay() > 0 && endDay.getDay() < 6)
                  weekEndCount -= 2;
              else if (endDay.getDay() == 6)
                  weekEndCount -= 1;

              if (beginDay.getDay() == 0) weekEndCount -= 1;
          } else {
              if (endDay.getDay() < 6) weekEndCount -= 1;

              if (beginDay.getDay() > 0) weekEndCount -= 1;
          }

          //根据调休设置，调整周末天数（排除调休日）
          WeekendsOff.forEach(function(offitem, i) {
            // console.log(offitem)
            let itemDay = new Date(offitem.split('-')[0] + "/" + offitem.split('-')[1] + "/" + offitem.split('-')[2]);
              //如果调休日在时间段区间内，且为周末时间（周六或周日），周末天数值-1
              if (itemDay.getTime() >= begin.getTime() && itemDay.getTime() <= end.getTime() && (itemDay.getDay() == 0 || itemDay.getDay() == 6))
                  weekEndCount -= 1;
          })
          //根据法定假日设置，计算时间段内周末的天数（包含法定假日）
          Holiday.forEach(function(itemHoliday, i) {
            let itemDay = new Date(itemHoliday.split('-')[0] + "/" + itemHoliday.split('-')[1] + "/" + itemHoliday.split('-')[2]);
              //如果法定假日在时间段区间内，且为工作日时间（周一至周五），周末天数值+1
              if (itemDay.getTime() >= begin.getTime() && itemDay.getTime() <= end.getTime() && itemDay.getDay() > 0 && itemDay.getDay() < 6)
                  weekEndCount += 1;
          })
      }

      //工作日 = 总天数 - 周末天数（包含法定假日并排除调休日）
      return days - weekEndCount;
  },
  /**
   * 深拷贝对象
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  deepCopy (data) {
    let result = {}
    try {
      result = JSON.parse(JSON.stringify(data))
    } catch (error) {
      console.log(error)
    }
    return result
  },
  /*
   * blod 下载附件
   */
  _fileBold (response) {
    let fileName = decodeURI(response.headers['content-disposition'].substring(20, response.headers['content-disposition'].length))
    let url = window.URL.createObjectURL(new Blob([response.data]))
    let link = document.createElement('a')
    link.style.display = 'none'
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
  },
  /**
  *获取签名
  * @param data 需要加密的参数
   */
  makeSign (data) {
    // 参数传入timestamp
    data['timestamp'] = +new Date()
    // data['timestamp'] = 20190803112045
    let _array = []
    for (const key in data) {
      // key转换成小写
      let lowKey = key.toLowerCase()
      // value urlencode编码
      let ucodeValue = encodeURIComponent(data[key])
      _array.push(`${lowKey}=${ucodeValue}`)
    }
    // 字典序升序排列
    _array = _array.sort((a, b) => {
      let _boolean = a.split('=')[0] > b.split('=')[0]
      return _boolean ? 1 : -1
    })
    // 参数数组加入特有参数key
    _array.push('key=hhdjasWASDS12312BSDFS')
    // console.log(decodeURIComponent(_array.join('&')))
    let sign = thisVue.md5(decodeURIComponent(_array.join('&')))
    return sign.toUpperCase()
  },
  // 判断对象是否包含空属性 包括null
  objectHasEmptyV (object) {
    // 判断对象类型
    if (Object.prototype.toString.call(object) === '[object Object]') {
      if (!Object.values(object).length) {
        // throw  new Error("not need a empty object")
        return true
      } else if(Object.values(object).indexOf('') !== -1 || Object.values(object).indexOf(null) !== -1) {
        return true
      } else {
        return false
      }
    } else {
      throw new Error('need a Object')
    }
    // if(object.prototype.toString().call(object) === [Object ])
    // Object.values(object)
  },
  //获取字符串长度（汉字算两个字符，字母数字算一个）
  getByteLen(val) {
    var len = 0;
    for (var i = 0; i < val.length; i++) {
      var a = val.charAt(i);
      if (a.match(/[^\x00-\xff]/ig) != null) {
        len += 2;
      }
      else {
        len += 1;
      }
    }
    return len;
  },
  returnImg(url) {
    return this.filePath + url
  },
}
/** 
 *   格式化日期格式
*/
Date.prototype.Format = function (fmt) {
  var o = {
    'M+': this.getMonth() + 1, // 月份
    'd+': this.getDate(), // 日
    'h+': this.getHours(), // 小时
    'm+': this.getMinutes(), // 分
    's+': this.getSeconds(), // 秒
    'q+': Math.floor((this.getMonth() + 3) / 3), // 季度
    'S': this.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length))
  for (var k in o) { if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length))) }
  return fmt
}

// 去除字符串前面空格
String.prototype.LTrim = function () {
  return this.replace(/(^\s*)/g, '')
}
// 去除字符串后面空格
String.prototype.RTrim = function () {
  return this.replace(/(\s*$)/g, '')
}
//删除数组指定的某个元素
Array.prototype.remove = function (val) {
  let index = this.indexOf(val)
  console.log(index)
  if (index > -1) {
    this.splice(index, 1)
  }
}

export default common
