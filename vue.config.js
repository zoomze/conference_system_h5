module.exports = {
  outputDir: 'dist',
  publicPath: '/h5/',
  assetsDir: 'assets',
  // configureWebpack: config => {
  //   config.entry.app = ["babel-polyfill", "./src/main.js"];
  // },
  devServer: {
    proxy: {
      '/apis': {
        target: 'http://47.105.40.119:8080/app/',
        // target: 'http://113.62.180.44:2180/app/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {'^/apis' : ''},
      },
    }
  }
}